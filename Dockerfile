ARG DRUPAL_VERSION=latest
FROM bitnami/drupal:${DRUPAL_VERSION}

USER 0

## 'install_packages' is a Bitnami-created utility
RUN install_packages vim git tar rsync wget curl wait-for-it

WORKDIR /opt/bitnami/drupal

ENV DRUPAL_ENABLE_MODULES "field_permissions,dul_dbtrials"
ENV BITNAMI_DEBUG yes
# If you uncommented the SAML module above, use this line instead
# ENV DRUPAL_ENABLE_MODULES "field_permissions,r2t2_content_type,samlauth"

WORKDIR /docker-entrypoint-init.d
COPY docker-entrypoint-init.d ./
RUN chmod -R a+x /docker-entrypoint-init.d/*.sh

USER 1001

RUN composer config repositories.dul-drupal/dul_dbtrials git \
      https://gitlab.oit.duke.edu/dul-drupal/dul-dbtrials.git

# Using this module will allow Drupal admins to 
# restrict field access
RUN composer require 'drupal/field_permissions:^1.2'
RUN composer require 'drupal/samlauth'

ENV SHELL /bin/bash
